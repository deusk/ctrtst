#!/bin/bash

set -o pipefail
# Make change as needed.
# Sometimes, the latest is broken.
tag=''
# Set to 0 if setting up extras/AppStream repo manually.
extras=1

# No need to change those below.
success=0
fail=1
ver=$(grep -o '[0-9]\.[0-9]' /etc/redhat-release)
arch=$(uname -m)
name=$(basename $0)
name=${name%.*}
images=''
rhel=0
beta=0
brew=''
python='python3'

ctrbuild() {
	ctr="$1"
	pull="$2"

	if [[ "$pull" == '' ]]; then
		$ctr pull "brew-pulp-docker01.web.prod.ext.phx2.redhat.com:8888/$brew:$ver$tag"
		$ctr tag "brew-pulp-docker01.web.prod.ext.phx2.redhat.com:8888/$brew:$ver$tag" 'base-image'
	else
		cat > /etc/yum.repos.d/ctrtst-rhel7.repo <<-EOF
		[ctrtst-rhel7]
		name=ctrtst-rhel7
		baseurl=http://download-node-02.eng.bos.redhat.com/rel-eng/rhel-7/updates/latest-RHEL-7/compose/Server/$arch/os
		enabled=1
		gpgcheck=0
EOF
	fi
	if [[ "$beta" == '1' ]]; then
		if [[ "$pull" == '' ]]; then
			opt='-v /etc/yum.repos.d:/etc/yum.repos.d'
		else
			opt='-v /etc/yum.repos.d/ctrtst-rhel7.repo:/etc/yum.repos.d/ctrtst-rhel7.repo'
		fi
	else
		opt=''
	fi
	$ctr build $opt --rm --no-cache --force-rm -t httpd-systemd .
	$ctr run -d --name httpd httpd-systemd
	ip=$($ctr inspect httpd | sed -n 's/ *"IPAddress": "\(.*\)",/\1/p' | head -1)
	# Default systemd start timeout is 90 secs.
	secs=0
	while [ $secs -ne 90 ]; do
		sleep 5
		if [[ $(curl $ip) == 'Hello World!' ]]; then
			break
		fi
		secs=$(($secs + 5))
	done
	if [ $secs -eq 90 ]; then
		echo "- error: unexpected $ctr httpd server response." >&2
		exit $fail
	fi
	$ctr stop httpd
	$ctr rm httpd
	$ctr rmi httpd-systemd
	if $ctr images | grep httpd-systemd; then
		echo "- error: unexpected removing $ctr images." >&2
		exit $fail
	fi
}

if grep -q Beta /etc/redhat-release; then
	beta=1
fi

# Must run as root.
if [[ $(id -u) != "0" ]]; then
	echo "- error: unexpected user not root." >&2
	exit $fail
fi

echo '- start: set up CDN channels'
if [[ "$ver" =~ ^7\.[0-9] ]]; then
	rhel=7
	tree="EXTRAS-RHEL-$ver/latest-EXTRAS-$ver-RHEL-7/compose/Server"
	python='python'
	yum -y install python-devel
elif [[ "$ver" =~ ^8\.[0-9] ]]; then
	rhel=8
	tree='latest-RHEL-8/compose/AppStream'
	# Fool pip.
	ln -sf /usr/libexec/platform-python /usr/bin/python
	ln -sf /usr/libexec/platform-python /usr/bin/python3
else
	echo '- error: unexpected RHEL version.' >&2
	exit $fail
fi
# baseurl is unable to be quoted until RHBZ#1624056 is fixed.
if [[ "$extras" == '1' ]]; then
	cat >/etc/yum.repos.d/$name.repo <<-EOF
	[extras]
	name=extras
	baseurl=http://download-node-02.eng.bos.redhat.com/nightly/$tree/$arch/os/
	enabled=1
	gpgcheck=0
EOF
fi
if [[ "$rhel" != '7' ]]; then
	dnf module -y enable container-tools
fi
# Workaround RHBZ#1640284 to install tar.
yum install -y podman git tar

echo '- start: setup the host.'
# It usually has no product pem file until close to the GA.
if [[ "$beta" == "0" ]]; then
	subscription-manager unregister
	subscription-manager register --serverurl=subscription.rhn.stage.redhat.com:443/subscription --baseurl=cdn.redhat.com --username=hellokittyabcd --password atomic --auto-attach --force
fi
cp /etc/containers/registries.conf /etc/containers/registries.conf.orig
cat >/etc/containers/registries.conf <<'EOF'
[registries.search]
registries = ['registry.access.redhat.com']

[registries.insecure]
registries = ['brew-pulp-docker01.web.prod.ext.phx2.redhat.com:8888']
EOF
# Legacy names seems not work since RHEL-7.6.
if [[ "$arch" == 'aarch64' ]]; then
	image="rhel$rhel-$arch"
else
	image="rhel$rhel"
fi
# aarch64 not arch-aware in the brew until bug 1624042 is fixed.
if [[ "$arch" == 'aarch64' ]] && [[ "$rhel" == '7' ]]; then
	brew="rhel$rhel-aarch64"
else
	brew="rhel$rhel"
fi

# Make sure container storage is overlayfs.
if grep -q '^ *driver = "devicemapper"' /etc/containers/storage.conf; then
	rm -rf /var/lib/containers/storage/*
	sed -i 's/\( *driver = .*\)/# \1/' /etc/containers/storage.conf
	sed -i 's/\( *directlvm_device = .*\)/# \1/' /etc/containers/storage.conf
	sed -i 's/\( *xfs_nospace_max_retries = .*\)/# \1/' /etc/containers/storage.conf
	sed -i 's/.*override_kernel_check = .*/override_kernel_check = "true"/' /etc/containers/storage.conf
fi
# This registry is arch-aware.
podman pull "brew-pulp-docker01.web.prod.ext.phx2.redhat.com:8888/$brew:$ver$tag"
podman tag "brew-pulp-docker01.web.prod.ext.phx2.redhat.com:8888/$brew:$ver$tag" base-image
if [[ "$beta" == "0" ]]; then
	repo=$(podman run --rm base-image yum repolist | grep -o 'rhel-[^/]*')
	if [[ "$repo" == ''  ]]; then
		echo '- error: unable to obtain repos inside containers.' >&2
		exit $fail
	fi
	subscription-manager repos --disable=* --enable=$repo
fi

echo '- start: check the base image.'
keys=$(podman run --rm base-image bash -c 'for i in $(rpm -qa); do rpm -qi $i | grep Signature | grep "Key ID "; done' | wc -l)
pkgs=$(podman run --rm base-image rpm -qa | grep -v gpg-pubkey- | wc -l)
if [[ "$keys" != "$pkgs" ]]; then
	echo '- error: unexpected package signatures for the base image.' >&2
	exit $fail
fi
# To see if it is posible to get the previous released base image.
if podman pull $image; then
	podman run --rm $image rpm -qa | sort -n >$name-old.lst
	podman run --rm base-image rpm -qa | sort -n>$name-new.lst
	echo '- info: the previous released vs new base image:'
	diff -u $name-old.lst $name-new.lst
fi

echo '- start: run a runc testsuite.'
cd ~
if [ ! -d runctst ]; then
	git clone git://fedorapeople.org/home/fedora/caiqian/public_git/runctst.git
fi
if [[ "$rhel" == "7" ]]; then
	yum install -y gcc ${python}-setuptools expect skopeo
	curl -O https://bootstrap.pypa.io/get-pip.py
	python get-pip.py
	pip install psutil
else
	yum install -y "http://download-node-02.eng.bos.redhat.com/brewroot/packages/python-psutil/5.4.3/5.el8+7/$arch/python3-psutil-5.4.3-5.el8+7.$arch.rpm"
	yum install -y expect skopeo
	# Have to watch and see if runc run path will be changed.
	#sed -i 's/runc-ctrs/runc/' runctst/runctst.py
fi
cd ~/runctst
# Fake a tty if needed.
if ! unbuffer ${python} runctst.py; then
	exit $fail
fi
# Workarond RHBZ#1609161.
if uname -r | grep -vq '4\.14\.0-'; then
	# Run for rootless.
	useradd -m test
	mkdir ~test/runctst
	cp runctst.py ~test/runctst
	# Workaround RHBZ#1627819.
	rm -rf /run/containers
	if ! su - test -c "cd runctst; unbuffer ${python} runctst.py"; then
		exit $fail
	fi
fi

echo '- start: run docker/podman build.'
if [[ "$beta" == 0 ]]; then
	opt="--disablerepo=* --enablerepo=$repo"
else
	opt=''
fi
cat >Dockerfile <<EOF
FROM base-image

RUN yum -y install $opt httpd
RUN yum clean all; systemctl enable httpd
RUN echo "Hello World!" > /var/www/html/index.html

STOPSIGNAL SIGRTMIN+3
EXPOSE 80
CMD ["/usr/sbin/init"]
EOF
if [[ "$rhel" == '7'  ]]; then
	yum install -y docker
	# Reset to overlayfs if needed.
	if grep -q '^ *STORAGE_DRIVER=devicemapper' /etc/sysconfig/docker-storage-setup; then
		echo >/etc/sysconfig/docker-storage-setup
		systemctl stop docker
		container-storage-setup --reset
		rm -rf /var/lib/docker/*
	fi
	# Restart regardless to activate registries.
	systemctl restart docker
	ctrbuild docker
fi
# Fake a container running to catch regressions.
podman run -d --name sleepd base-image sleep 1000
ctrbuild podman
podman stop sleepd
podman rm sleepd

# Test the latest RHEL7 base image on next major release.
if [[ "$rhel" == '8' ]] && [[ "$arch" != 'aarch64' ]]; then
	podman pull brew-pulp-docker01.web.prod.ext.phx2.redhat.com:8888/rhel7
	podman tag brew-pulp-docker01.web.prod.ext.phx2.redhat.com:8888/rhel7 base-image
	ctrbuild podman nopull
fi

# Only test docker on RHEL7 while podman on future versions.
# Right now, podman devicemapper is broken in RHEL7.X due to RHBZ#1623944.
if [[ "$rhel" == "7" ]]; then
	echo '- start: check devicemapper storage.'
	# Use swap for an extra device for DM. It should be off in runctst above.
	if grep -q swap /proc/cmdline && grep -q swap /etc/fstab; then
		group=$(lvs | sed -n 's/ *swap *\([^ ]*\) .*/\1/p')
		lvremove -f "/dev/$group/swap"
		# Make the system rebootable without swap.
		sed -i '/.* swap */d' /etc/fstab
		kernel=$(grubby --default-kernel)
		# Due to grubby bugs (RHBZ#1629054, #1629053), it need to be manually fixed to avoid un-rebootable systems.
		grubby --remove-args="rd.lvm.lv=$group/swap" --update-kernel=$kernel
	fi
	systemctl stop docker
	container-storage-setup --reset
	rm -rf /var/lib/docker/*
	echo 'STORAGE_DRIVER=devicemapper' >/etc/sysconfig/docker-storage-setup
	systemctl start docker
	ctrbuild docker
	systemctl stop docker
# Podman devicemapper support is broken indefinitely. See RHBZ#1625394.
#else
	# Fake a block device using loopback.
#	dd of=$name.blk if=/dev/zero bs=1G count=10
#	losetup -f $name.blk
#	lo=$(losetup -a | sed -n "s/\([^:]*\).*$name.blk)/\1/p")
#	rm -rf /var/lib/containers/storage/*
#	sed -i 's/.*driver = .*/driver = "devicemapper"/' /etc/containers/storage.conf
#	sed -i "s;.*directlvm_device = .*;directlvm_device = '$lo';" /etc/containers/storage.conf
#	sed -i 's/.*xfs_nospace_max_retries = .*/xfs_nospace_max_retries = "10"/' /etc/containers/storage.conf
#	sed -i 's/\( *override_kernel_check = .*\)/# \1/' /etc/containers/storage.conf
#	ctrbuild podman
#	podman run -d --name nospace base-image sleep 60
#	for i in $(podman exec nospace ls -d /sys/block/dm-*); do
#		if podman exec nospace cat "$i/dm/name" | grep 'container-[0-9]'; then
#			dm=${i##/sys/block/}
#		fi
#	done
#	retries=$(podman exec nospace cat /sys/fs/xfs/$dm/error/metadata/ENOSPC/max_retries)
#	if [[ "$retries" != "10" ]]; then
#		echo "unexpected max retries is $retries." >&2
#		exit $fail
#	fi
#	podman stop nospace
#	podman rm nospace
#	lvremove storage/thinpool
#	pvremove /dev/loop0
#	losetup -d lo
#	rm $name.blk
fi

echo '- start: run a container-selinux testsuite.' 
cd ~
if [ ! -d csetst ]; then
	git clone https://gitlab.com/deusk/csetst.git
fi
cd ~/csetst
if ! python csetst.py; then
	echo '- error: unexpected the container-selinux testsuite failed.' >&2
	exit $fail
fi

echo '- start: check SELinux.'
out=$(ausearch -m AVC -ts recent 2>&1)
if [[ "$out" != '<no matches>' ]]; then
	echo "- error: unexpected AVC denial: $out" >&2
fi
echo "- pass: $name"
exit $success
